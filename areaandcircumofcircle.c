#include<stdio.h>
#include<math.h>
float get_radius()
{
float radius;
printf("Enter the radius\n");
scanf("%f",&radius);
return radius;
}
float compute_circum(float radius)
{
float circum;
circum=2*M_PI*radius;
return circum;
}
float compute_area(float radius)
{
float area;
area=M_PI*radius*radius;
return area;
}
void output(float radius,float circum)
{
printf("The circum of circle with radius %f is %f\n",radius,circum);
}
void output_circum(float radius,float area)
{
printf("The area of circle with radius %f is %f\n",radius,area);
}
int main()
{
float radius,area,circum;
radius=get_radius();
area=compute_area(radius);
circum=compute_circum(radius);
output(radius,area);
output_circum(radius,circum);
return 0;
}