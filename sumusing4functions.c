#include<stdio.h>

int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int get_input()
{
    int b;
    printf("Enter a number\n");
    scanf("%d",&b);
    return b;
}
int compute(int a,int b)
{
    int c;
    c=a+b;
    return c;
}
void output(int a,int b,int c)
{
    printf("%d+%d=%d\n",a,b,c);
}
int main(int a,int b,int c)
{
    a=input();
    b=get_input();
    c=compute(a,b);
    output(a,b,c);
    return 0; 
}
